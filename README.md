Here is a quick how-to for building/using docker images:

    1. Run ./build-env.sh as a regular user, after cloning the repo. It will create all necessary environmental variables for the build
    2. Run sudo docker-compose build (Docker compose must be installed. See: https://docs.docker.com/compose/)
    3. After the build (successfully) completes, adjust nginx configuration (eg. update hostnames, add necessary configuration) in the ./etc/nginx/conf.d/default.conf file. Also, update docker-compose.yml (check line 31). Current configuration works for Magento 2.
    4. Run: sudo docker-compose up -d

To connect to specific (running) container, run: sudo docker-compose exec *container_name* /bin/bash

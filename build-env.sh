#! /bin/sh
#
# build.sh
# Copyright (C) 2018 Milan Jovanovic <mjovanovic@deployinc.com>
#
# Distributed under terms of the MIT license.
#

FILENAME=".env"
GID=$(id -g)
if [ -e "$FILENAME" ] && [ -f "$FILENAME" ]; then
    echo "$FILENAME already setup"
    exit
fi
if [ ! -e "$FILENAME" ]; then
    echo "LOCAL_USER_ID=$UID" >>"$FILENAME"
    echo "LOCAL_GROUP_ID=$GID" >>"$FILENAME"
    echo "$FILENAME creation successfuly completed"
else
    echo "$FILENAME exists, but it's not a file. Aborting"
fi

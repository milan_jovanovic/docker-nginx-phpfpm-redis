#! /bin/sh
#
# php-entrypoint.sh
# Copyright (C) 2018 Milan Jovanovic <m.jovanovic.rs@gmail.com>
#
# Distributed under terms of the MIT license.
#

USER_ID=${LOCAL_USER_ID:-9001}
GROUP_ID=${LOCAL_GROUP_ID:-9001}
echo "Creating user with UID: $USER_ID"
groupadd -g $USER_ID docker
useradd -d /var/www/html -u $USER_ID -g $GROUP_ID -M -N -s /bin/bash docker

set -e

exec /usr/sbin/php-fpm7.0 --nodaemonize
